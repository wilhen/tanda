This is the instruction to run the program on your local machine.
Laravel framework v5.3 is used for this code challenge.

1. Set up localhost server, for example, MAMP, XAMPP or WAMP.
    - Make sure the PHP version is 5.6! Below 5.6 the code cannot be runned.
2. Turn on the MySQL server.
3. Extract the zip file.
3. Open the Tanda-Code-Challenge folder and put the SQL dump (Tanda.SQL) into your MySQL database.
4. Use terminal or command line to run the program. Direct to /Tanda-Code-Challenge/Tanda
5. Run the following command to run the program.
    * php artisan serve --port=8080 (this allows you to run the code on your localhost with port 8080)
        - You can modify the port number as you wish but remember to modify the port number on the ruby testing file.
    * to stop the server you can press control+c 
    * modify the database configuration of the project on /Tanda/config/database.php and /Tanda/.env file
        - Modify the port number, host, username, database name and password
        - on .env:
            + DB_HOST -- for the database host
            + DB_PORT -- for the database port
            + DB_DATABASE -- for the database name
            + DB_USERNAME -- for the database username
            + DB_PASSWORD -- for the database password
        - on database.php:
            + find the database configuration with 'mysql' as the key and replace the configuration according
              to the local MySQL server configuration.
        


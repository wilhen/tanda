<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::get('/devices','PingsController@getAll');
Route::post('/clear_data','PingsController@deleteAll');
Route::post('/{device_id}/{epoch_time}','PingsController@store');
Route::get('/{device_id}/{from}/{to?}','PingsController@filter');
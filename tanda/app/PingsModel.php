<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PingsModel extends Model
{
    //
    protected $table='pings';
    protected $fillable=['epoch_time','device_id'];
    public $timestamps=false;
    public $incrementing=false;

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\PingsModel;
use Carbon\Carbon;

class PingsController extends Controller
{
    //
    public function getAll(){
        $todos=PingsModel::all();
        $temp=array();
        foreach($todos as $todo){
            if(!in_array($todo->device_id,$temp)){
                array_push($temp,$todo->device_id);
            } 
        }
        return $temp;
    }


    public function store($device_id,$epoch_time){
        $device_id=explode('/',$device_id);
        $epoch_time=explode('/',$epoch_time);
        $todos=PingsModel::where('device_id',$device_id[0])
                          ->where('epoch_time',Carbon::createFromTimestamp($epoch_time[0]))
                          ->get();
        if(count($todos)==0){
            PingsModel::create([
                'device_id'=>$device_id[0],
                'epoch_time'=>Carbon::createFromTimestamp($epoch_time[0]) 
            ]);
            return ['message'=>'Success'];
        }else{
            return response()->json(['message'=>'Already exists!'],403);
        }
    }

    public function deleteAll(){
        $todos=PingsModel::where('device_id','like','%%');
        $todos->delete();
        return ['message'=>'Success'];
    }

    public function filter($device_id,$from,$to=null){
        $device_id=explode('/',$device_id)[0];
        $from=explode('/',$from)[0];
        $to=explode('/',$to)[0];
        
        $array=array();
        // echo Carbon::createFromFormat('Y-m-d H', $from.' 0');
        if($to!==''){
            if(is_numeric($from)==false){
                $from=Carbon::createFromFormat('Y-m-d H', $from.' 0');
                // echo($from);
            }else{
                $from=Carbon::createFromTimestamp($from);
                // echo($from);
            }
            if(is_numeric($to)==false){
                $to=Carbon::createFromFormat('Y-m-d H', $to.' 0');
                // echo($to);
            }else{
                $to=Carbon::createFromTimestamp($to);
                // echo($to);
            }

            $todos=PingsModel::where('epoch_time','>=',$from)
                             ->where('epoch_time','<',$to);
            
            if($device_id!=='all'){
                $todos->where('device_id',$device_id);
                // $todos->get();
                foreach($todos->get() as $todo){
                    array_push($array,strtotime($todo->epoch_time));
                }
                return $array;
            }else if($device_id=='all'){
                // $todos->get();
                
                foreach($todos->get() as $todo){           
                    if(!array_key_exists($todo->device_id,$array)){
                        $temp=array();
                        array_push($temp,strtotime($todo->epoch_time));
                        $array[$todo->device_id]=$temp;
                    }else{
                        $content=$array[$todo->device_id];
                        // print_r($content);
                        array_push($content,strtotime($todo->epoch_time));
                        $array[$todo->device_id]=$content;
                    }
                    
                }
                return $array;
            }
  
        }else if($to==''){
            if(is_numeric($from)==true){
                return response()->json(['message'=>'Invalid Argument'],400);
            }
            $fromFormatted=Carbon::createFromFormat('Y-m-d H', $from.' 0');
            $to=Carbon::createFromFormat('Y-m-d H', $from.' 24');
            // echo($to);
            $todos=PingsModel::where('epoch_time','>=',$fromFormatted)
                             ->where('epoch_time','<',$to); //because it will be different date
                           
            if($device_id!=='all'){
                $todos->where('device_id',$device_id);
                // $todos->get();
                foreach($todos->get() as $todo){
                    array_push($array,strtotime($todo->epoch_time));
                }
                return $array;
            }else if($device_id=='all'){
                // echo('allnow');
                // echo($todos->get());
                // $todos->get();
                
                foreach($todos->get() as $todo){
                    
                    if(!array_key_exists($todo->device_id,$array)){
                        $temp=array();
                        array_push($temp,strtotime($todo->epoch_time));
                        $array[$todo->device_id]=$temp;
                    }else{
                        $content=$array[$todo->device_id];
                        // echo($todo->device)
                        // print_r($content);
                        array_push($content,strtotime($todo->epoch_time));
                        $array[$todo->device_id]=$content;
                    }
                
            }
            return $array;
            
                            
                            
            
        }
    }


    }
}
